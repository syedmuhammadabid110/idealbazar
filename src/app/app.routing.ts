import { CheckoutDetailsComponent } from './checkout-details/checkout-details.component';
import { Routes, RouterModule } from '@angular/router';
import { CartComponent } from './cart/cart.component';
import { SignupComponent } from './signup/signup.component';
import { LoginComponent } from './login/login.component';


const APP_ROUTES: Routes = [
    { path: 'login', component: LoginComponent },
    { path: 'signup', component: SignupComponent },
    { path: 'cart', component: CartComponent },
    { path: '', component: CheckoutDetailsComponent },
    { path: '**', redirectTo: 'login', pathMatch: 'full' },
];

export const routing = RouterModule.forRoot(APP_ROUTES);